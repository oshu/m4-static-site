m4_define(`_MY_FILENAME',`stuff.html')m4_dnl
m4_include(`modules/globaldefinitions.m4')m4_dnl
m4_define(`_MY_TITLE',`Stuff')m4_dnl
m4_include(`modules/htmlheader.m4')m4_dnl

<header>
    <h1>_MY_ENTITY_NAME - _MY_TITLE</h1>
</header>

<nav>
    m4_include(`modules/menu.m4')
</nav>

<section>
    <h2>_MY_TITLE</h2>
    <p>This is where I will tell you some stuff</p>
</section>

m4_include(`modules/htmlfooter.m4')m4_dnl
