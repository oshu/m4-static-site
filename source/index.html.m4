m4_define(`_MY_FILENAME',`index.html')m4_dnl
m4_include(`modules/globaldefinitions.m4')m4_dnl
m4_define(`_MY_TITLE',_MY_ENTITY_NAME)m4_dnl
m4_include(`modules/htmlheader.m4')m4_dnl

<header>
    <h1>_MY_TITLE</h1>
</header>

<nav>
m4_include(`modules/menu.m4')m4_dnl
</nav>

<section>
    <h2>Goals</h2>
    <p>The goal of this project is to evaluate the m4 macro processor for static site generation.</p>
    <p>In no particular order</p>
    <ul>
        <li>Add pages without editing the Makefile</li>
        <li>Generate a menu</li>
        <li>Generate a &ldquo;last updated&rdquo; datestamp</li>
        <li>Generate headers</li>
        <li>Generate footers</li>
        <li>Minimal coding / Maximum reuse</li>
    </ul>
</section>

m4_include(`modules/htmlfooter.m4')m4_dnl
