m4_define(`_MY_STYLESHEET',`css/style.min.css')m4_dnl
m4_define(`_MY_ENTITY_NAME',`M4 Static Site Experiment')m4_dnl
m4_define(`_MY_COPYRIGHT_NOTICE',`&#169; 2019' _MY_ENTITY_NAME)m4_dnl
m4_define(`__MENULINK',`<li><a href="$1"m4_ifelse(_MY_FILENAME,$1,` class="active"',`')>$2</a></li>')m4_dnl
