SOURCE_DIR := source
MODULES_DIR := $(SOURCE_DIR)/modules
CSS_SOURCE_DIR := style
IMAGE_SOURCE_DIR := images

HTML_TEMPLATES := $(notdir $(shell find $(SOURCE_DIR) -name '*.html.m4'))
HTML_MODULES := $(notdir $(shell find $(MODULES_DIR) -name '*.m4'))
HTML_OUTPUT_FILES := $(HTML_TEMPLATES:.html.m4=.html)
CSS_INPUT_FILES := $(notdir $(shell find $(CSS_SOURCE_DIR) -name '*.css'))
CSS_OUTPUT_FILES := $(CSS_INPUT_FILES:.css=.min.css)

TARGET_DIR := public
TARGET_CSS_DIR := $(TARGET_DIR)/css
TARGET_IMAGE_DIR := $(TARGET_DIR)/images

VPATH = $(SOURCE_DIR) $(IMAGE_SOURCE_DIR) $(MODULES_DIR) $(TARGET_DIR) $(TARGET_CSS_DIR) $(CSS_SOURCE_DIR)
DATESTAMP := $(shell date -u)
M4_OPTIONS := -P -I $(SOURCE_DIR) -D _BUILD_DATETIME="$(DATESTAMP)"
SED_COMPRESS_HTML := sed -e '/^$$/d' -e 's/^  \+//'
SED_COMPRESS_CSS := sed -e '/^\/\*/d' -e 's/^  \+//'

.PHONY : all clean images html style

all : html style
html : $(HTML_OUTPUT_FILES)
images :
style : $(CSS_OUTPUT_FILES)


%.html : %.html.m4 $(HTML_MODULES)
	test -d $(TARGET_DIR) || mkdir -p $(TARGET_DIR)
	m4 $(M4_OPTIONS) $(@).m4 | $(SED_COMPRESS_HTML) > $(TARGET_DIR)/$(@)


%.min.css : %.css
	test -d $(TARGET_CSS_DIR) || mkdir -p $(TARGET_CSS_DIR)
	$(SED_COMPRESS_CSS) $(CSS_SOURCE_DIR)/$(@:.min.css=.css) > $(TARGET_CSS_DIR)/$(@)


clean :
	rm -rf $(TARGET_DIR)
