# m4-static-site

Experimenting with static generation with make and m4

View the [generated site](https://m4-static-site.netlify.com). 

[![Netlify Status](https://api.netlify.com/api/v1/badges/f33bf932-8931-45c0-bc15-c82beb3734b1/deploy-status)](https://app.netlify.com/sites/m4-static-site/deploys)
